package xyz.angkrang.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.amqpbridge.AmqpBridge;
import io.vertx.amqpbridge.AmqpConstants;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonObject;

public class AmqpConsumerVerticle extends AbstractVerticle {

	private Logger log = LoggerFactory.getLogger(AmqpConsumerVerticle.class);

	@Override
	public void start() throws Exception {

		AmqpBridge bridge = AmqpBridge.create(vertx);
		bridge.start(ConfigVerticle.AMQP_URL, ConfigVerticle.AMQP_PORT, ConfigVerticle.AMQP_USERNAME,
				ConfigVerticle.AMQP_PASSWORD, queueConnected -> {
					MessageConsumer<JsonObject> consumer = bridge.createConsumer(ConfigVerticle.AMQP_QUEUE_CONSUMER);
					consumer.handler(message -> {
						consumer.pause();
						vertx.executeBlocking(block -> {
							JsonObject amqpMsgPayload = message.body();
							String msg = amqpMsgPayload.getValue(AmqpConstants.BODY).toString();

							// TODO proses...

							block.complete();
						}, false, finishBlock -> {
							consumer.resume();
						});
					});
				});

	}

}
