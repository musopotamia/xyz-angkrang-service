package xyz.angkrang.service;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.amqpbridge.AmqpBridge;
import io.vertx.amqpbridge.AmqpConstants;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.MessageProducer;
import io.vertx.core.json.JsonObject;

public class AmqpProducerVerticle extends AbstractVerticle {

	private Logger log = LoggerFactory.getLogger(AmqpProducerVerticle.class);

	AmqpBridge bridge;
	HashMap<String, MessageProducer<JsonObject>> producerPool = new HashMap<>();

	@Override
	public void start() throws Exception {
		bridge = AmqpBridge.create(vertx);
		bridge.start(ConfigVerticle.AMQP_URL, ConfigVerticle.AMQP_PORT, ConfigVerticle.AMQP_USERNAME,
				ConfigVerticle.AMQP_PASSWORD, queueConnected -> {
					vertx.eventBus().consumer(ConfigVerticle.EB_AMQP_PRODUCER, message -> {
						sendMessage(message.body().toString(), "queue_name");
					});
				});
	}

	private void sendMessage(String message, String destination) {
		MessageProducer<JsonObject> producer = producerPool.get(destination);
		if (producer == null) {
			producer = bridge.createProducer(destination);
			producerPool.put(destination, producer);
		}
		JsonObject request = new JsonObject();
		request.put(AmqpConstants.BODY, message);
		producer.send(request);
		log.info("Send message to queue {}", destination);
	}
}
