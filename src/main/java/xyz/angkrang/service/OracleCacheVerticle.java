package xyz.angkrang.service;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import scala.collection.mutable.HashMap;
import xyz.angkrang.lib.setting.GlobalSetting;

/**
 * Load & Sync Cache every 2 minutes
 */
public class OracleCacheVerticle extends AbstractVerticle {

	private final Logger log = LoggerFactory.getLogger(OracleCacheVerticle.class);
	private JDBCClient client;
	private HazelcastInstance hz;

	public OracleCacheVerticle(JDBCClient client, HazelcastInstance hz) {
		this.client = client;
		this.hz = hz;
	}

	@Override
	public void start() throws Exception {
		log.info("CacheVerticle starting....");

		// HeTempStore heTempStore = new HeTempStore();
		// HeSettingSchedulerStore heSettingSchedulerStore = new
		// HeSettingSchedulerStore();
		// HeDataCollectionStore heDataCollectionStore = new HeDataCollectionStore();
		// HeProtocolCommandStore heProtocolCommandStore = new HeProtocolCommandStore();
		// HeDataStateStore heDataStateStore = new HeDataStateStore();

		// load
		final long start = System.currentTimeMillis();

		// Future<Void> futureHeSettingScheduleStore = Future.future();
		// heSettingSchedulerStore.LoadAll(this.client, this.hz,
		// futureHeSettingScheduleStore);

		// Future<Void> futureHeDataCollectionStore = Future.future();
		// heDataCollectionStore.LoadAll(this.client, futureHeDataCollectionStore);

		// Future<Void> futureHeTempStore = Future.future();
		// heTempStore.LoadAll(this.client, this.hz, futureHeTempStore);

		// Future<Void> futureHeProtocolCommand = Future.future();
		// heProtocolCommandStore.LoadAll(this.client, futureHeProtocolCommand);

		// Future<Void> futureHeDataStateCommand = Future.future();
		// heDataStateStore.LoadAll(this.client, this.hz, futureHeDataStateCommand);

		// sync
		// vertx.setPeriodic(ConfigVerticle.CACHE_REFRESH_TIME, id -> {
		// heSettingSchedulerStore.SyncCache(this.client);
		// });
		// vertx.setPeriodic(ConfigVerticle.CACHE_REFRESH_TIME, id -> {
		// heDataCollectionStore.SyncCache(this.client);
		// });
		// vertx.setPeriodic(ConfigVerticle.CACHE_REFRESH_TIME, id -> {
		// heTempStore.SyncCache(this.client, this.hz);
		// });
		// vertx.setPeriodic(ConfigVerticle.CACHE_REFRESH_TIME, id -> {
		// heProtocolCommandStore.SyncCache(this.client);
		// });
		// vertx.setPeriodic(ConfigVerticle.CACHE_REFRESH_TIME, id -> {
		// heDataStateStore.SyncCache(this.client, this.hz);
		// });

		Map<String, Date> limitReq = this.hz.getMap("LIMIT_REQUEST");

		vertx.setPeriodic(ConfigVerticle.CACHE_REFRESH_TIME, id -> {
			Map<String, Date> limitReqBac = new java.util.HashMap<>(limitReq);
			for (String key : limitReqBac.keySet()) {
				Date now = new java.util.Date();
				long diff = now.getTime() - limitReq.get(key).getTime();
				long diffMinutes = diff / (60 * 1000);

				if (diffMinutes > GlobalSetting.limitRequest) {
					limitReq.remove(key);
				}
			}
		});

		// check if all load finish, triggering deploy task scheduler
		// CompositeFuture.all(Arrays.asList(futureHeTempStore,
		// futureHeDataStateCommand)).setHandler(ar -> {
		// if (ar.succeeded()) {
		// log.info("load data cache done in {} ms", (System.currentTimeMillis() -
		// start));
		// }
		// });

	}
}
