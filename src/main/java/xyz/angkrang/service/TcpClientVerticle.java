package xyz.angkrang.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.net.NetClient;
import io.vertx.core.net.NetSocket;

public class TcpClientVerticle extends AbstractVerticle{

	private Logger log = LoggerFactory.getLogger(TcpClientVerticle.class);

	@Override
	public void start() throws Exception {
		   NetClient tcpClient = vertx.createNetClient();

	        tcpClient.connect(ConfigVerticle.TCP_CLIENT_PORT, ConfigVerticle.TCP_CLIENT_HOST,
	            new Handler<AsyncResult<NetSocket>>(){

	            @Override
	            public void handle(AsyncResult<NetSocket> result) {
	                NetSocket socket = result.result();
	                /*write*/
	                socket.write("GET / HTTP/1.1\r\nHost: jenkov.com\r\n\r\n");
	                /*read*/
	                socket.handler(new Handler<Buffer>(){
	                    @Override
	                    public void handle(Buffer buffer) {
	                        log.info("Received data: " + buffer.length());

	                        /*read tcp*/
	                        log.info(buffer.getString(0, buffer.length()));
	                    }
	                });
	            }
	        });
	}
}
