package xyz.angkrang.service;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;

public class ConfigVerticle extends AbstractVerticle {

	public static String AMQ_USERNAME;
	public static String AMQ_PASSWORD;
	public static String AMQ_URL;

	public static String AMQP_USERNAME;
	public static String AMQP_PASSWORD;
	public static String AMQP_URL;
	public static Integer AMQP_PORT;
	public static String AMQP_QUEUE_CONSUMER;

	public static Integer TCP_SERVER_PORT;
	public static Integer TCP_CLIENT_PORT;
	public static String TCP_CLIENT_HOST;

	public static String QUEUE_RESPONSE_RAW;
	public static String QUEUE_CACHE_FLUSH;
	public static String QUEUE_SUMMARY;
	public static Integer CACHE_REFRESH_TIME;

	public static String TELEGRAM_BOT_TOKEN;
	public static String TELEGRAM_BOT_EB_SEND_MESSAGE;
	public static String TELEGRAM_BOT_Q_SEND_MESSAGE;
	public static String TELEGRAM_BOT_RESP_SEND_MESSAGE;
	public static String TELEGRAM_BOT_DETAIL;
	public static String TELEGRAM_BOT_DETAIL_FLUSH;
	
	public static String DB_DRIVER_CLASS;
	public static Integer DB_MAX_POOL_SIZE;
	public static String DB_URL;
	public static String DB_USERNAME;
	public static String DB_PASSWORD;

	public static String DB_MYSQL_DRIVER_CLASS;
	public static String DB_MYSQL_URL;
	public static String DB_MYSQL_HOST;
	public static Integer DB_MYSQL_PORT;
	public static Integer DB_MYSQL_MAX_POOL_SIZE;
	public static String DB_MYSQL_USERNAME;
	public static String DB_MYSQL_PASSWORD;
	public static String DB_MYSQL_DATABASE;

	public static String EB_DB_INSERT;
	public static String EB_RAWDATA;
	public static String EB_DB_RAWDATA;

	public static String EB_AMQP_PRODUCER;

	public static Boolean RAWDATA_ACTIVE;

	public ConfigVerticle(JsonObject config) {

		AMQ_USERNAME = config.getString("activemq.username", "amicon");
		AMQ_PASSWORD = config.getString("activemq.password", "MQ666");
		// AMQ_URL = config.getString("activemq.url", "tcp://10.14.158.179:61616");
		AMQ_URL = config.getString("activemq.url", "tcp://10.14.153.42:61616");

		AMQP_USERNAME = config.getString("amqp.username", "amicon");
		AMQP_PASSWORD = config.getString("amqp.password", "MQ666");
		AMQP_URL = config.getString("amqp.url", "10.14.153.42");
		AMQP_PORT = config.getInteger("amqp.port", 5672);
		AMQP_QUEUE_CONSUMER = config.getString("amqp.queue.consumer", "amqp.consumer");

		TCP_SERVER_PORT = config.getInteger("tcp.server.port", 10000);
		TCP_CLIENT_PORT = config.getInteger("tcp.client.port", 10000);
		TCP_CLIENT_HOST = config.getString("tcp.client.host", "localhost");

		TELEGRAM_BOT_TOKEN = config.getString("telegram.bot.token", "483546536:AAHiIqKp7r5us4q6cWP7duggfTYW2fjo_KI");
		TELEGRAM_BOT_EB_SEND_MESSAGE = config.getString("telegram.bot.token.eb.send.message", "send.message");
		TELEGRAM_BOT_Q_SEND_MESSAGE = config.getString("telegram.bot.token.q.send.message", "send.message");
		TELEGRAM_BOT_RESP_SEND_MESSAGE = config.getString("telegram.bot.token.resp.send.message", "resp.send.message");
		TELEGRAM_BOT_DETAIL = config.getString("telegram.bot.detail", "telegram_bot_detail");
		TELEGRAM_BOT_DETAIL_FLUSH = config.getString("telegram.bot.detail.flush", "telegram_bot_detail_flush");
		
		DB_DRIVER_CLASS = config.getString("jdbc.driverClass", "oracle.jdbc.OracleDriver");
		DB_MAX_POOL_SIZE = config.getInteger("jdbc.maxPoolSizeDb", 10);

		DB_URL = config.getString("jdbc.url", "jdbc:oracle:thin:@//10.14.152.137:1521/dbmars");
		DB_USERNAME = config.getString("jdbc.user", "AMICON");
		DB_PASSWORD = config.getString("jdbc.password", "HijrahtoArab");

		DB_MYSQL_DRIVER_CLASS = config.getString("jdbc.driverClass", "com.mysql.jdbc.Driver");
		DB_MYSQL_URL = config.getString("jdbc.url", "jdbc:mysql:thin:@//10.14.205.18:3306/amicon2");
		DB_MYSQL_HOST = config.getString("jdbc.mysql.host", "10.14.205.18");
		DB_MYSQL_PORT = config.getInteger("jdbc.mysql.port", 3306);
		DB_MYSQL_MAX_POOL_SIZE = config.getInteger("jdbc.mysql.max.pool", 10);
		DB_MYSQL_USERNAME = config.getString("jdbc.mysql.user", "amicon2");
		DB_MYSQL_PASSWORD = config.getString("jdbc.mysql.password", "j00s");
		DB_MYSQL_DATABASE = config.getString("jdbc.mysql.database", "amicon2");

		EB_DB_INSERT = config.getString("eb.db.insert.cache", "eb.db.insert.cache");
		EB_RAWDATA = config.getString("eb.rawdata", "eb.rawdata");
		EB_DB_RAWDATA = config.getString("eb.db.rawdata", "eb.db.rawdata");

		EB_AMQP_PRODUCER = config.getString("eb.amqp.producer", "eb.amqp.producer");

		RAWDATA_ACTIVE = config.getBoolean("rawdata.active", true);
		QUEUE_RESPONSE_RAW = config.getString("queue.response.raw", "meter.response.raw");
		QUEUE_CACHE_FLUSH = config.getString("queue.cache.flush", "cache.flush");
		QUEUE_SUMMARY = config.getString("queue.summary", "summary.flush");

		CACHE_REFRESH_TIME = config.getInteger("cache.refresh.time", (2 * 60 * 1000)); // 2 minutes

	}

}
