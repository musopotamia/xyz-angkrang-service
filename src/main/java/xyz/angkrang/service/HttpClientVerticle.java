package xyz.angkrang.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.amqpbridge.AmqpBridge;
import io.vertx.amqpbridge.AmqpConstants;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageProducer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import xyz.angkrang.lib.http.obj.HttpReq;
import xyz.angkrang.lib.http.obj.HttpResp;

public class HttpClientVerticle extends AbstractVerticle {

	private Logger log = LoggerFactory.getLogger(HttpClientVerticle.class);

	WebClient client;
	HashMap<String, MessageProducer<JsonObject>> producerPool = new HashMap<>();

	@Override
	public void start() throws Exception {
		WebClientOptions options = new WebClientOptions().setUserAgent("My-App/1.2.3");
		options.setKeepAlive(false);
		client = WebClient.create(vertx, options);
		
	}

	private HttpResp Request(HttpReq req) {
		try {
			HttpResp response = new HttpResp();
			response.setHttpReq(req);
			switch (req.getTypeHttpReq()) {
			case GET:
				HttpRequest<Buffer> getReq = client.get(req.getUrlPort(), req.getUrlHost(), req.getUrlAction());
				if (!req.getUrlMapParams().isEmpty() && req.getUrlMapParams().size() > 0) {
					for (String key : req.getUrlMapParams().keySet()) {
						getReq.addQueryParam(key, req.getUrlMapParams().get(key));
					}
				}
				
				if (null != req.getUrlTimeout() && req.getUrlTimeout() > 0) {
					getReq.timeout(req.getUrlTimeout());
				}
				
				getReq.send(ar -> {
					if (ar.succeeded()) {
						// Obtain response
						HttpResponse<Buffer> resp = ar.result();
						response.setAsyncResult(ar);
						log.info("Received response with status code" + resp.statusCode());
					} else {
						response.setAsyncResult(ar);
						log.info("Something went wrong " + ar.cause().getMessage());
					}

				});
				break;

			default:
				break;
			}

			return response;
		} catch (Exception e) {
			log.info("Request failed, {}", req, e);
			throw e;
		}

	}

}
