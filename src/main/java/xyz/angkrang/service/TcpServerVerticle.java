package xyz.angkrang.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.net.NetServer;
import io.vertx.core.net.NetSocket;

public class TcpServerVerticle extends AbstractVerticle {
	private Logger log = LoggerFactory.getLogger(TcpServerVerticle.class);

	@Override
	public void start() throws Exception {
		NetServer server = vertx.createNetServer();

		server.connectHandler(new Handler<NetSocket>() {

			@Override
			public void handle(NetSocket netSocket) {
				log.info("Incoming connection!");

				netSocket.handler(new Handler<Buffer>() {

					@Override
					public void handle(Buffer inBuffer) {
						log.info("incoming data: " + inBuffer.length());

						String data = inBuffer.getString(0, inBuffer.length());

						log.info("data: " + data);

						Buffer outBuffer = Buffer.buffer();
						
						/*response*/
						String response = "response...";
						outBuffer.appendString(response);

						netSocket.write(outBuffer);
					}

				});
			}
		});

		server.listen(ConfigVerticle.TCP_SERVER_PORT);

	}

}
