package xyz.angkrang.service;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import io.vertx.ext.asyncsql.AsyncSQLClient;
import io.vertx.ext.asyncsql.MySQLClient;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;

public class MainVerticle extends AbstractVerticle {

	private final Logger log = LoggerFactory.getLogger(MainVerticle.class);

	@Override
	public void start() throws Exception {

		log.info("Starting Cache Collector Application...");

		System.setProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory");
		System.setProperty("Log4jContextSelector", "org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");

		vertx.deployVerticle(new ConfigVerticle(config()), res -> {
			JDBCClient client = JDBCClient.createShared(vertx,
					new JsonObject().put("url", ConfigVerticle.DB_URL)
							.put("driver_class", ConfigVerticle.DB_DRIVER_CLASS)
							.put("max_pool_size", ConfigVerticle.DB_MAX_POOL_SIZE)
							.put("user", ConfigVerticle.DB_USERNAME).put("password", ConfigVerticle.DB_PASSWORD));


			JsonObject mySQLClientConfig = new JsonObject().put("host", ConfigVerticle.DB_MYSQL_HOST).put("port", ConfigVerticle.DB_MYSQL_PORT)
					.put("maxPoolSize", ConfigVerticle.DB_MYSQL_MAX_POOL_SIZE)
					.put("username", ConfigVerticle.DB_MYSQL_USERNAME)
					.put("password", ConfigVerticle.DB_MYSQL_PASSWORD)
					.put("database", ConfigVerticle.DB_MYSQL_DATABASE);
			
			AsyncSQLClient mySQLClient = MySQLClient.createShared(vertx, mySQLClientConfig, "MySQLPool1");
			
			Set<HazelcastInstance> instances = Hazelcast.getAllHazelcastInstances();
			HazelcastInstance hz = instances.stream().findFirst().get();
			
			vertx.deployVerticle(new TelegramVerticle(ConfigVerticle.TELEGRAM_BOT_TOKEN));
			
//			vertx.deployVerticle(new LoadCacheHe1Verticle(client));
//			vertx.deployVerticle(new LoadCacheHe2Verticle(client));
//			vertx.deployVerticle(new CacheVerticle(client,hz));
//
//			vertx.deployVerticle(new FlushCacheConsumerVerticle());
//			vertx.deployVerticle(new FlushCacheDatabaseVerticle(client,hz));
//			vertx.deployVerticle(new SummaryVerticle(mySQLClient));
			
			if(ConfigVerticle.RAWDATA_ACTIVE) {
//				vertx.deployVerticle(new RawDataConsumerVerticle(mySQLClient));
			}
		});

	}

}
