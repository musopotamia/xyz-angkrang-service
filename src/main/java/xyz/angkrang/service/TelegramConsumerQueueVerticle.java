package xyz.angkrang.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.WorkerExecutor;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.Json;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import xyz.angkrang.lib.enu.TypeTelegramRequest;
import xyz.angkrang.lib.model.AuditTrail;
import xyz.angkrang.lib.model.DetailTelegramBot;
import xyz.angkrang.lib.model.TelegramRequest;
import xyz.angkrang.lib.model.TelegramResponse;
import xyz.angkrang.lib.model.TelegramRequest;

public class TelegramConsumerQueueVerticle extends AbstractVerticle {

	private final Logger log = LoggerFactory.getLogger(TelegramConsumerQueueVerticle.class);
	private HazelcastInstance hz;
	private WebClient client;
	private IMap<String, DetailTelegramBot> detailTelegramBot;
	private EventBus evb = vertx.eventBus();

	public TelegramConsumerQueueVerticle(HazelcastInstance hz2) {
		this.hz = hz2;
		this.detailTelegramBot = hz.getMap(ConfigVerticle.TELEGRAM_BOT_Q_SEND_MESSAGE);
	}

	@Override
	public void start() throws Exception {
		if (detailTelegramBot.size() > 0) {
			log.info("load data cache done, deploying consumer");

			final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ConfigVerticle.AMQ_USERNAME,
					ConfigVerticle.AMQ_PASSWORD, ConfigVerticle.AMQ_URL);

			final Connection connection = connectionFactory.createConnection();
			connection.start();
			final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			final Destination destination = session.createQueue(ConfigVerticle.TELEGRAM_BOT_Q_SEND_MESSAGE);
			final MessageConsumer consumer = session.createConsumer(destination);
			consumer.setMessageListener(message -> {
				if (message instanceof TextMessage) {
					final TextMessage textMessage = (TextMessage) message;
					try {
						TelegramRequest request = Json.decodeValue(textMessage.getText(), TelegramRequest.class);
						switch (request.getTypeTelegramRequest()) {
						case SendMessage:
							sendMessage(request);
							break;
							
						default:
							log.error("unhandle message : " + textMessage.getText());
							break;
						}
					} catch (JMSException e) {
						log.error("error get message : " , e);
					}
				}
			});
		} else {
			log.error("load data cache failed..");
		}
	}

	private void sendMessage(TelegramRequest task) {
		TelegramResponse resp = new TelegramResponse();
		resp.setTypeTelegramRequest(TypeTelegramRequest.SendMessage);

		AuditTrail auditTrail = new AuditTrail();
		auditTrail.setAksi("telegram consumer queue");
		auditTrail.setProsesDari("send message");
		auditTrail.setWaktu(System.currentTimeMillis());
		task.putListAuditTrail(auditTrail);

		resp.setKeterangan(Json.encode(task));

		log.info("{} {} {} | process data..", task.getTrxid(), task.getTelegramId(), task.getMessageText());

		if (null != task.getFromTelegramBot() && !task.getFromTelegramBot().isEmpty()
				&& detailTelegramBot.containsKey(task.getFromTelegramBot())) {
			String secretToken = detailTelegramBot.get(task.getFromTelegramBot()).getTokenBot();
			if (null != secretToken && !secretToken.isEmpty()) {
				HttpRequest<Buffer> getReq = client.get(TypeTelegramRequest.UrlAsli.getValue() + secretToken,
						resp.getTypeTelegramRequest().getValue());
				getReq.addQueryParam("parse_mode", "markdown");
				getReq.addQueryParam("chat_id", task.getTelegramId());
				getReq.addQueryParam("text", task.getMessageText());
				getReq.send(ar -> {
					int statusCode = 0;
					String message = "";
					if (ar.succeeded()) {
						HttpResponse<Buffer> response = ar.result();
						statusCode = response.statusCode();
						log.info("{} {} {} | Received response with status code : {}", task.getTrxid(),
								task.getTelegramId(), task.getMessageText(), statusCode);
						resp.setResponseStatus(true);
					} else {
						message = ar.cause().getMessage();
						log.info("{} {} {} | Something went wrong : {}", task.getTrxid(), task.getTelegramId(),
								task.getMessageText(), message);
						resp.setResponseStatus(false);
					}
					resp.setBody(Json.encode(ar.result()));
					evb.send(ConfigVerticle.TELEGRAM_BOT_RESP_SEND_MESSAGE, Json.encode(resp));
				});
			} else {
				log.error("{} {} {} | failed to process, token telegram bot is null / is empty", task.getTrxid(),
						task.getTelegramId(), task.getMessageText());
			}
		} else {
			log.error("{} {} {} | failed to process, telegram bot is null / is empty / hz contain key false",
					task.getTrxid(), task.getTelegramId(), task.getMessageText());
		}
	}

}
