package xyz.angkrang.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.request.BaseRequest;
import com.pengrad.telegrambot.response.BaseResponse;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.net.NetClient;
import io.vertx.core.net.NetSocket;
import xyz.angkrang.lib.enu.TypeProcess;

public class TelegramVerticle extends AbstractVerticle {

	private Logger log = LoggerFactory.getLogger(TelegramVerticle.class);

	String botToken;

	/**
	 * @param log
	 * @param botToken
	 */
	public TelegramVerticle(String botToken) {
		this.botToken = botToken;
	}

	@Override
	public void start() throws Exception {
		/* Creating your bot */
		TelegramBot bot = new TelegramBot(botToken);

		/*
		 * You can build bot with custom OkHttpClient, for specific timeouts or
		 * interceptors.
		 */
		// TelegramBot bot = new
		// TelegramBot.Builder("BOT_TOKEN").okHttpClient(client).build();

		
		;
	}

	
}
