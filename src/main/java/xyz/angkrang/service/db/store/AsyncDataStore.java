package xyz.angkrang.service.db.store;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.asyncsql.AsyncSQLClient;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLConnection;
import xyz.angkrang.lib.db.DBRequest;
import xyz.angkrang.lib.db.DBResponse;
import xyz.angkrang.lib.db.DataJsonInsert;
import xyz.angkrang.service.db.util.QueryGenerator;

public class AsyncDataStore {
	private final Logger log = LoggerFactory.getLogger(AsyncDataStore.class);

	public DBResponse LoadAll(DBRequest param, AsyncSQLClient client, Future<Void> future, HazelcastInstance hz) {
		// Set<HazelcastInstance> instances = Hazelcast.getAllHazelcastInstances();
		// HazelcastInstance hz = instances.stream().findFirst().get();
		DBResponse dBResponse = new DBResponse();
		dBResponse.setRequestId(param.getRequestId());
		Map<String, Map<String, Object>> mapResult = hz.getMap(param.getTable().toUpperCase());
		Map<String, Object> mapResponse = new LinkedHashMap<>();
		mapResponse.put("table_key", param.getKey());
		mapResponse.put("table_name", param.getTable());
		mapResponse.put("table_column", param.getColumns());

		String query = QueryGenerator.generateSelect(param.getColumns(), param.getTable(), param.getWheres());
		log.info("load query : {}", query);
		try {
			client.getConnection(res -> {
				if (res.succeeded()) {
					SQLConnection connection = res.result();
					connection.query(query, res2 -> {
						if (res2.succeeded()) {
							mapResponse.put("db_connection", true);
							ResultSet rs = res2.result();
							String action = String.format("load %s ...", param.getTable().toUpperCase());
							log.info(action);
							mapResponse.put("action", action);

							HashMap<String, Object> pureTable = new HashMap<>();
							for (JsonArray line : rs.getResults()) {
								StringBuilder ids = new StringBuilder();
								for (int i = 0; i < param.getColumns().size(); i++) {
									pureTable.put(param.getColumns().get(i), line.getValue(i));
									// log.info("{} {} : {}", param.getTable().toUpperCase(),
									// param.getColumns().get(i), line.getValue(i));
									for (String key : param.getKey()) {
										if (param.getColumns().get(i).equalsIgnoreCase(key)) {
											ids.append(line.getString(i));
										}
									}
								}
								mapResult.put(ids.toString(), pureTable);
								// log.info("{} value : {}", ids, pureTable);
							}
							dBResponse.setResult(mapResult);

							log.info("{} load {} data", param.getTable(), mapResult.size());
						} else {
							// Failed to get connection - deal with it
							mapResponse.put("error_message", String.format("failed execute loadAll %s, cause %s",
									param.getTable(), res.cause()));
							log.info(mapResponse.get("error_message").toString());
						}

					});
				} else {
					// Failed to get connection - deal with it
					mapResponse.put("error_message",
							String.format("failed get connection loadAll %s, cause %s", param.getTable(), res.cause()));
					log.info(mapResponse.get("error_message").toString());
				}
				future.complete();
			});
		} catch (Exception e) {
			mapResponse.put("error_message",e.getMessage());
			log.info("err:", e);
		}
		dBResponse.setResponse(mapResponse);
		return dBResponse;
	}

	public DBResponse sync(DBRequest param, AsyncSQLClient client, Boolean DB, Boolean cache) {
		DBResponse result = new DBResponse();
		result.setRequestId(param.getRequestId());
		Map<String, Object> response = new HashMap<>();
		Set<HazelcastInstance> instances = Hazelcast.getAllHazelcastInstances();
		HazelcastInstance hz = instances.stream().findFirst().get();
		Map<String, Map<String, Object>> mapResult = hz.getMap(param.getTable().toUpperCase());

		try {

			client.getConnection(conn -> {
				if (conn.failed()) {
					log.error("conn {} failed, {}", param.getTable(), conn.cause().getMessage());
					return;
				}
				final SQLConnection connection = conn.result();
				log.info("conn {} open", param.getTable());

				switch (param.getTypeAction()) {
				case INSERTUPDATE:
					if (cache) {
						/* cache */
						String msg = "";
						StringBuilder ids = new StringBuilder();
						for (String key : param.getKey()) {
							ids.append(param.getParams().get(key));
							mapResult.put(ids.toString(), param.getParams());
							msg = String.format("add cache %s, %s success", param.getTable(), param.getParams());
							response.put("cacheStatus", true);
							response.put("cacheMessage", msg);
							log.info(msg);
						}
					}
					if (DB) {
						/* DB */
						String query = QueryGenerator.generateSelectCount(param.getTable(), param.getWheres());
						connection.query(query, res2 -> {
							Integer count = 0;
							if (res2.succeeded()) {
								ResultSet rs = res2.result();
								for (JsonArray line : rs.getResults()) {
									count = line.getInteger(0);
								}

								log.info("count {} where {} -> {}", param.getTable().toUpperCase(),
										param.getWheres().toString(), count);

								if (count > 0) {
									DataJsonInsert dataJsonUpdate = new DataJsonInsert(param.getTable(),
											param.getParams(), param.getWheres());
									String queryUpdate = QueryGenerator.generateUpdate(dataJsonUpdate);
									log.info("execute query {}", queryUpdate);

									connection.update(queryUpdate, updateResult -> {
										String msg1 = "";
										if (updateResult.succeeded()) {
											msg1 = String.format("update %s, %s success", param.getTable(),
													param.getParams());
											response.put("status", true);
											response.put("message", msg1);
											log.info(msg1);
										} else {
											msg1 = String.format("update %s, %s failed : %s", param.getTable(),
													param.getParams(), updateResult.cause());
											response.put("status", false);
											response.put("message", msg1);
											log.error(msg1);
										}
									});
								} else {
									DataJsonInsert dataInsert = new DataJsonInsert(param.getTable().toUpperCase(),
											param.getParams());
									String query1 = QueryGenerator.generateInsert(dataInsert);
									log.info("execute query {}", query1);
									connection.update(query1, insertResult -> {
										String msg1 = "";
										if (insertResult.succeeded()) {
											msg1 = String.format("insert %s, %s success", param.getTable(),
													param.getParams());
											response.put("status", true);
											response.put("message", msg1);
											log.info(msg1);
										} else {
											msg1 = String.format("insert %s, %s failed : %s", param.getTable(),
													param.getParams(), insertResult.cause());
											response.put("status", false);
											response.put("message", msg1);
											log.error(msg1);
										}
									});
								}
							}
						});
					}
					break;
				case INSERT:
					if (cache) {
						/* cache */
						StringBuilder ids = new StringBuilder();
						String msg = "";
						for (String key : param.getKey()) {
							ids.append(param.getParams().get(key));
						}
						if (!ids.toString().isEmpty()) {
							mapResult.put(ids.toString(), param.getParams());
							msg = String.format("add cache %s, %s success", param.getTable(), param.getParams());
							response.put("cacheStatus", true);
							response.put("cacheMessage", msg);
							log.info(msg);
						} else {
							msg = String.format("add cache %s, %s failed, key null", param.getTable(),
									param.getParams());
							response.put("cacheStatus", false);
							response.put("cacheMessage", msg);
							log.info(msg);
						}
					}
					if (DB) {
						/* DB */
						Map<String, Object> data = new HashMap<>(param.getParams());
						DataJsonInsert dataInsert = new DataJsonInsert(param.getTable().toString(), data);
						String query = QueryGenerator.generateInsert(dataInsert);
						log.info("execute query {}", query);

						connection.update(query, insertResult -> {
							String msg1 = "";
							if (insertResult.succeeded()) {
								msg1 = String.format("insert %s, %s success", param.getTable(), param.getParams());
								response.put("status", true);
								response.put("message", msg1);
								log.info(msg1);
							} else {
								msg1 = String.format("insert %s, %s failed : %s", param.getTable(), param.getParams(),
										insertResult.cause());
								response.put("status", false);
								response.put("message", msg1);
								log.error(msg1);
							}

						});
					}
					break;

				case UPDATE:
					Integer count = 0;
					Map<String, Object> dataUpdate = new HashMap<>(param.getParams());
					Map<String, Object> whereUpdate = new HashMap<>(param.getWheres());
					if (cache) {
						/* cache */
						String msg = "";
						if (mapResult.size() > 0) {
							String idU = "";
							Map<String, Object> mapU = new HashMap<>();
							for (String k : mapResult.keySet()) {
								Map<String, Object> mapValue = mapResult.get(k);
								count = 0;
								for (String l : whereUpdate.keySet()) {
									if (mapValue.containsKey(l)) {
										if (mapValue.get(l).equals(whereUpdate.get(l))) {
											count++;
										}
									}
								}
								if (count == whereUpdate.size()) {
									idU = k;
									mapU.putAll(mapValue);
									break;
								}
							}
							for (String k : dataUpdate.keySet()) {
								mapU.put(k, dataUpdate.get(k));
							}
							mapResult.put(idU, mapU);

							msg = String.format("update cache %s, %s success", param.getTable(), param.getParams());
							response.put("cacheStatus", true);
							response.put("cacheMessage", msg);
							log.info(msg);
						} else {
							msg = String.format("update cache %s, %s failed, key null", param.getTable(),
									param.getParams());
							response.put("cacheStatus", false);
							response.put("cacheMessage", msg);
							log.info(msg);
						}
					}
					if (DB) {
						/* DB */
						DataJsonInsert dataJsonUpdate = new DataJsonInsert(param.getTable(), dataUpdate, whereUpdate);
						String queryUpdate = QueryGenerator.generateUpdate(dataJsonUpdate);
						log.info("execute query {}", queryUpdate);

						connection.update(queryUpdate, updateResult -> {
							String msg1 = "";
							if (updateResult.succeeded()) {
								msg1 = String.format("update %s, %s success", param.getTable(), param.getParams());
								response.put("status", true);
								response.put("message", msg1);
								log.info(msg1);
							} else {
								msg1 = String.format("update %s, %s failed : %s", param.getTable(), param.getParams(),
										updateResult.cause());
								response.put("status", false);
								response.put("message", msg1);
								log.error(msg1);
							}
						});
					}
					break;
				case DELETE:
					log.info("delete");
					if (cache) {
						/* cache */
						String msg = "";
						if (mapResult.size() > 0) {
							String idU = "";
							for (String k : mapResult.keySet()) {
								Map<String, Object> mapValue = mapResult.get(k);
								count = 0;
								for (String l : param.getWheres().keySet()) {
									if (mapValue.containsKey(l)) {
										if (mapValue.get(l).equals(param.getWheres().get(l))) {
											count++;
										}
									}
								}
								if (count == param.getWheres().size()) {
									idU = k;
									break;
								}
							}

							mapResult.remove(idU);

							msg = String.format("delete cache %s, %s success", param.getTable(), param.getWheres());
							response.put("cacheStatus", true);
							response.put("cacheMessage", msg);
							log.info(msg);
						} else {
							msg = String.format("delete cache %s, %s failed, key null", param.getTable(),
									param.getParams());
							response.put("cacheStatus", false);
							response.put("cacheMessage", msg);
							log.info(msg);
						}
					}
					if (DB) {
						/* DB */
						DataJsonInsert dataJsonDelete = new DataJsonInsert(param.getTable(), new HashMap<>(),
								param.getWheres());
						String queryDelete = QueryGenerator.generateDelete(dataJsonDelete);
						connection.update(queryDelete, deleteResult -> {
							String msg1 = "";
							if (deleteResult.succeeded()) {
								msg1 = String.format("delete %s, %s success", param.getTable(), param.getParams());
								response.put("status", true);
								response.put("message", msg1);
								log.info(msg1);
							} else {
								msg1 = String.format("delete %s, %s failed : %s", param.getTable(), param.getParams(),
										deleteResult.cause());
								response.put("status", false);
								response.put("message", msg1);
								log.error(msg1);
							}
						});
					}
					break;
				default:
					break;
				}

				connection.close(resultdb -> {
					if (resultdb.failed()) {
						log.error("error close connection {} : {}", param.getTable(), resultdb.cause().getMessage());
						throw new RuntimeException(resultdb.cause());
					} else {
						log.info("conn {} close", param.getTable());
					}
				});
			});
		} catch (Exception e) {
			response.put("status", false);
			response.put("message", e.getMessage());
			response.put("exception", e);
		}

		result.setResponse(response);
		return result;
	}

}
