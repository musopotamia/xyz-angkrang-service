package xyz.angkrang.service.db.util;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import xyz.angkrang.lib.db.DataJsonInsert;
import xyz.angkrang.service.ConfigVerticle;

public class QueryGenerator {

	public static String generateInsertFlush(DataJsonInsert data) {
		StringBuilder query = new StringBuilder();
		StringBuilder param = new StringBuilder();
		query.append("insert into ");
		query.append(ConfigVerticle.DB_USERNAME);
		query.append(".");
		query.append(data.getTableName());
		query.append(" (");
		param.append(" values (");

		boolean firstParams = true;
		for (String key : data.getData().keySet()) {
			Object value = data.getData().get(key);
			if (value == null) {
				continue;
			}
			if (!firstParams) {
				query.append(",");
				param.append(",");
			}
			if (value instanceof String) {
				if (((String) value).contains("TO_DATE")) {
					param.append(value);
				} else {
					param.append("'" + value + "'");
				}
			} else if (value instanceof Number) {
				if (value instanceof Double) {
					param.append(doubleToStringWithoutScientificNotation((Double) value));
				} else {
					param.append(value);
				}
			}
			query.append(key);
			firstParams = false;
		}

		query.append(") ");
		param.append(")");

		return query.toString() + param.toString();
	}

	public static JsonArray generateWhere(DataJsonInsert data) {
		JsonArray wheres = new JsonArray();
		for (String where : data.getWhere().keySet()) {
			Object value = data.getWhere().get(where);
			if (value instanceof String) {
				if (((String) value).contains("TO_DATE")) {
					wheres.add(value);
				} else {
					wheres.add("'" + value + "'");
				}
			} else if (value instanceof Number) {
				if (value instanceof Double) {
					wheres.add(doubleToStringWithoutScientificNotation((Double) value));
				} else {
					wheres.add(value);
				}
			}
		}
		return wheres;
	}

	public static String generateUpdateFlush(DataJsonInsert data) {
		StringBuilder query = new StringBuilder();

		query.append("update ");
		query.append(ConfigVerticle.DB_USERNAME);
		query.append(".");
		query.append(data.getTableName());
		query.append(" set ");

		int i = 1;
		for (String key : data.getData().keySet()) {
			Object value = data.getData().get(key);

			query.append(key).append(" = ");

			if (value instanceof String) {
				if (((String) value).contains("TO_DATE")) {
					query.append(value);
				} else {
					query.append("'" + value + "'");
				}
			} else if (value instanceof Number) {
				if (value instanceof Double) {
					query.append(doubleToStringWithoutScientificNotation((Double) value));
				} else {
					query.append(value);
				}
			}

			if (i < data.getData().size()) {
				query.append(", ");
			}
			i++;
		}
		if (!data.getWhere().isEmpty()) {
			query.append(" where ");
			for (String key : data.getWhere().keySet()) {
				query.append(key).append(" =? ");

				// Object value = data.getWhere().get(key);
				//
				// if (value instanceof String) {
				// if (((String) value).contains("TO_DATE")) {
				// query.append(value);
				// } else {
				// query.append("'" + value + "'");
				// }
				// } else if (value instanceof Number) {
				// if (value instanceof Double) {
				// query.append(doubleToStringWithoutScientificNotation((Double) value));
				// } else {
				// query.append(value);
				// }
				// }
			}
		}

		return query.toString();
	}

	private static String doubleToStringWithoutScientificNotation(Double d) {
		DecimalFormat df = new DecimalFormat("#");
		df.setMaximumFractionDigits(8);
		return df.format(d);
	}

	public static String generateSelect(List<String> koloms, String table, Map<String, Object> where) {
		StringBuilder query = new StringBuilder();
		query.append("SELECT ");
		for (int i = 0; i < koloms.size(); i++) {
			query.append(koloms.get(i).toUpperCase() + " ");
			if (i < (koloms.size() - 1)) {
				query.append(", ");
			}
		}
		query.append("FROM " + table.toUpperCase() + " ");
		if (where.size() > 0) {
			query.append("WHERE ");
			String eWhere = Json.encode(where).replace("//", "").replace("{", "").replace("}", "").replace(":", "=")
					.replace(",", " And ");
			query.append(eWhere);
		}
		return query.toString();
	}

	public static String generateInsert(DataJsonInsert data) {
		StringBuilder query = new StringBuilder();
		StringBuilder param = new StringBuilder();
		query.append("insert into ");
		query.append(data.getTableName());
		query.append(" (");

		param.append(" values (");
		int i = 1;
		for (String key : data.getData().keySet()) {
			query.append(key);
			if (data.getData().get(key).toString().toUpperCase().equals("NOW()")
					|| data.getData().get(key).toString().toUpperCase().contains("STR_TO_DATE")) {
				param.append(data.getData().get(key));
			} else {
				param.append("'" + data.getData().get(key) + "'");
			}
			if (i < data.getData().size()) {
				query.append(",");
				param.append(",");
			}
			i++;
		}
		query.append(") ");
		param.append(")");

		return query.toString() + param.toString();
	}

	public static String generateInsertUpdate(DataJsonInsert data) {
		StringBuilder query = new StringBuilder();

		return query.toString();
	}

	public static String generateUpdate(DataJsonInsert data) {
		StringBuilder query = new StringBuilder();

		query.append("update ");
		query.append(data.getTableName());
		query.append(" set ");

		int i = 1;
		for (String key : data.getData().keySet()) {
			query.append(key).append(" = '").append(data.getData().get(key)).append("'");
			if (i < data.getData().size()) {
				query.append(", ");
			}
			i++;
		}
		// if (!data.getWhere().isEmpty()) {
		// query.append(" where ");
		// for (String key : data.getWhere().keySet()) {
		// query.append(key).append(" = '").append(data.getWhere().get(key)).append("'
		// ");
		// }
		// }
		if (!data.getWhere().isEmpty()) {
			query.append(" where ");
			for (String key : data.getWhere().keySet()) {
				String value = data.getWhere().get(key).toString();
				String text = value.trim().substring(value.lastIndexOf(" ") + 1);
				query.append(key).append(" = '");
				if (text.toUpperCase().indexOf("AND") > -1 || text.toUpperCase().indexOf("OR") > -1) {
					query.append(value.substring(0, value.lastIndexOf(" ")));
					query.append("' ");
					query.append(text.toUpperCase()).append(" ");
				} else {
					query.append(data.getWhere().get(key));
					query.append("' ");
				}

			}
		}

		return query.toString();
	}

	public static String generateDelete(DataJsonInsert data) {
		StringBuilder sb = new StringBuilder();
		sb.append("delete from ");
		sb.append(data.getTableName().toUpperCase());

		if (!data.getWhere().isEmpty()) {
			sb.append(" where ");
			for (String key : data.getWhere().keySet()) {
				String value = data.getWhere().get(key).toString();
				String text = value.trim().substring(value.lastIndexOf(" ") + 1);
				sb.append(key).append(" = '");
				if (text.toUpperCase().indexOf("AND") > -1 || text.toUpperCase().indexOf("OR") > -1) {
					sb.append(value.substring(0, value.lastIndexOf(" ")));
					sb.append("' ");
					sb.append(text.toUpperCase()).append(" ");
				} else {
					sb.append(data.getWhere().get(key));
					sb.append("' ");
				}

			}
		}

		return sb.toString();
	}

	public static String generateSelectCount(String table, Map<String, Object> wheres) {
		StringBuilder query = new StringBuilder();
		query.append("select count(*) from ").append(table.toUpperCase()).append(" ");
		if (wheres.size() > 0) {
			query.append(" where ");
			for (String key : wheres.keySet()) {
				String value = wheres.get(key).toString();
				String text = value.trim().substring(value.lastIndexOf(" ") + 1);
				query.append(key).append(" = '");
				if (text.toUpperCase().indexOf("AND") > -1 || text.toUpperCase().indexOf("OR") > -1) {
					query.append(value.substring(0, value.lastIndexOf(" ")));
					query.append("' ");
					query.append(text.toUpperCase()).append(" ");
				} else {
					query.append(wheres.get(key));
					query.append("' ");
				}
			}
		}

		return query.toString();
	}
}
