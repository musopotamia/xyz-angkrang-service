package xyz.angkrang.service.db.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	public static String meterFormatToQueryDbFormat(String s, String type) {
		if (s == null) {
			return null;
		}
		String date = meterFormatToDbFormat(s, type);
		return appendToDate(date, type);
	}

	public static String dateToQueryDbFormat(Date d, String type) {
		if (d == null) {
			return null;
		}
		String date = dateToDbFormat(d, type);
		return appendToDate(date, type);
	}

	public static String appendToDate(String date, String type) {
		if (date == null || type == null) {
			return null;
		}
		String oracleFormat = null;
		if (type.equals("DATE")) {
			oracleFormat = "YYYY-MM-DD";
		} else if (type.equals("TIME")) {
			oracleFormat = "YYYY-MM-DD HH24:MI:SS";
		}
		return "TO_DATE('" + date + "','" + oracleFormat + "')";
	}

	public static String meterFormatToDbFormat(String dateMeter, String type) {
		Date d = meterFormatToDate(dateMeter);
		if (d == null) {
			return null;
		}
		return dateToDbFormat(d, type);
	}

	public static Date meterFormatToDate(String dateMeter) {
		if (dateMeter == null || dateMeter.isEmpty()) {
			return null;
		}
		DateFormat formatToDate = new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy");
		try {
			Date d = formatToDate.parse(dateMeter);
			return d;
		} catch (ParseException e) {
			return null;
		}
	}

	public static String dateToDbFormat(Date dateMeter, String type) {
		DateFormat formatToDB = null;
		if (type.equals("DATE")) {
			formatToDB = new SimpleDateFormat("yyyy-MM-dd");
		} else if (type.equals("TIME")) {
			formatToDB = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		} else {
			return null;
		}
		return formatToDB.format(dateMeter);

	}

	public static Date toISO8061(String s) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		try {
			return format.parse(s);
		} catch (ParseException e) {
			return null;
		}
	}

	public static String toStringFormat(Date d, String dateFormat) {
		DateFormat format = new SimpleDateFormat(dateFormat);
		return format.format(d);
	}

	public static String toISO8061(Date d) {
		if (d == null) {
			return null;
		}
		return DateUtil.toStringFormat(d, "yyyy-MM-dd'T'HH:mm:ssZ");
	}

	public static String toDLMSFormat(Date d) {
		if (d == null) {
			return null;
		}
		return DateUtil.toStringFormat(d, "EEE MMM d HH:mm:ss z yyyy");
	}

}
