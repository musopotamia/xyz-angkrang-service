package xyz.angkrang.service;

import com.hazelcast.config.Config;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

public class DebugAppCache {

	public static void main(String[] args) {
		Config hazelcastConfig = new Config();
		ClusterManager mgr = new HazelcastClusterManager(hazelcastConfig);
		VertxOptions option = new VertxOptions().setClusterManager(mgr);
		Vertx.clusteredVertx(option, result -> {
			if (result.succeeded()) {
				Vertx vertx = result.result();
				vertx.deployVerticle(new MainVerticle());
			}
		});
	}

}
